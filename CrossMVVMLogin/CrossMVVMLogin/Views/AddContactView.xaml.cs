﻿using CrossMVVMLogin.Models;
using CrossMVVMLogin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrossMVVMLogin.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddContactView : ContentPage
	{
		public AddContactView ()
		{
			InitializeComponent ();
            BindingContext = new AddContactViewModel(Contact());
		}

        public Contact Contact()
        {
            return new Contact()
            {
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Email = txtEmail.Text,
                Address = txtAddress.Text,
                Age = txtAge.Text,
                PhoneNumber = txtPhoneNumber.Text,
                PhotoUrl = ""
            };
            
        }

        private void ResetInput()
        {
            
        }
    }
}