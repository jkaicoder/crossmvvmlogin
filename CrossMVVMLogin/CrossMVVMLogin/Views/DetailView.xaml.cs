﻿using CrossMVVMLogin.Models;
using CrossMVVMLogin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrossMVVMLogin.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailView : ContentPage
	{
        public DetailViewModel DetailViewModel { get; }
		public DetailView (Contact contact)
		{
			InitializeComponent ();
            BindingContext = DetailViewModel = new DetailViewModel();
            DetailViewModel.SetData(contact);
        }

        public DetailView()
        {
            InitializeComponent();
        }
    }
}