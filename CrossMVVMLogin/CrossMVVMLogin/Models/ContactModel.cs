﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CrossMVVMLogin.Models
{

    public class ContactModel
    {
        public static Contact _contact;
        private SQLiteConnection conn;

        public ContactModel()
        {
            conn = DependencyService.Get<ISQLite>().GetConnection();
            conn.CreateTable<Contact>();
        }

        public bool AddContact(Contact contact)
        {
            conn.Insert(contact);
            return true;
        }
    }
}
