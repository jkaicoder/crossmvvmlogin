﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossMVVMLogin.Models
{
    public class Data
    {
        public static IEnumerable<Contact> GetContacts()
        {
            return new List<Contact>
            {
                new Contact() {FirstName = "JonKai", LastName = "Ann", Email = "kai@gmail.com", Age = "17", PhoneNumber = "2938379283", Address = "VietNam", PhotoUrl = "gmail"},
                new Contact() {FirstName = "Lee", LastName = "Nguyen", Email = "kai@gmail.com", Age = "17", PhoneNumber = "2938379283", Address = "VietNam", PhotoUrl = "test"},
                new Contact() {FirstName = "Kun", LastName = "Pachai", Email = "kai@gmail.com", Age = "17", PhoneNumber = "2938379283", Address = "VietNam", PhotoUrl = "c"},
                new Contact() {FirstName = "Kai", LastName = "Ja", Email = "kai@gmail.com", Age = "17", PhoneNumber = "2938379283", Address = "VietNam", PhotoUrl = "b"},
                new Contact() {FirstName = "Roller", LastName = "Larry", Email = "kai@gmail.com", Age = "17", PhoneNumber = "2938379283", Address = "VietNam", PhotoUrl = "d"}
            };
        }
    }
}
