﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossMVVMLogin.Models
{
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Age { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string PhotoUrl { get; set; }
        public string FullName => FirstName + " " + LastName;
    }
}
