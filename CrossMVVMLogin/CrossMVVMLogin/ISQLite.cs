﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossMVVMLogin
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
