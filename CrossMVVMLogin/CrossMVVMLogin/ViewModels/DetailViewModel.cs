﻿using CrossMVVMLogin.Models;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossMVVMLogin.ViewModels
{
    public class DetailViewModel : BaseViewModel
    {
        private Contact _contact;

        public Contact Contact
        {
            get => _contact;
            set
            {
                SetProperty(ref _contact, value);
            }
        }

        public DetailViewModel()
        {

        }

        public void SetData(Contact contact)
        {
            this._contact = contact;
            OnPropertyChanged("Contact");
            Title = Contact.FullName;
        }
    }
}
