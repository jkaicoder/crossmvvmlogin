﻿using CrossMVVMLogin.Models;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CrossMVVMLogin.ViewModels
{
    public class AddContactViewModel : BaseViewModel
    {
        private Command AddContact { get; }
        private Contact _contact;

        public AddContactViewModel(Contact contact)
        {
            AddContact = new Command(AddNewContact);
            this._contact = contact;
        }

        private void AddNewContact(object obj)
        {
            new ContactModel().AddContact(_contact);
        }
    }
}
