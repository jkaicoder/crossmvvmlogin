﻿using CrossMVVMLogin.Views;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CrossMVVMLogin.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private INavigation _navigation;
        private string _passWord;
        private string _email;

        public string Email
        {
            get => _email;
            set
            {
                if (_email != value)
                    _email = value;
                LoginCommand.ChangeCanExecute();
            }
        }
        public string PassWord
        {
            get => _passWord;
            set
            {
                if (_passWord != value)
                    _passWord = value;
                LoginCommand.ChangeCanExecute();
            }
        }
        public Command LoginCommand { get; }

        public LoginViewModel(INavigation navigation)
        {
            LoginCommand = new Command(LoginExcute, CanLogin);
            this._navigation = navigation;
        }

        private bool CanLogin(object arg)
        {
            return !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(PassWord);
        }

        private void LoginExcute(object obj)
        {
            if (Email.Equals("admin") && PassWord.Equals("123"))
            {
                _navigation.PushAsync(new MainView());
            }
            else
            {

            }
        }

        


    }
}
