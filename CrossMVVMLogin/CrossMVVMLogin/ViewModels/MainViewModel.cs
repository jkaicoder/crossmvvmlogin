﻿using CrossMVVMLogin.Models;
using CrossMVVMLogin.Views;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CrossMVVMLogin.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private INavigation _navigation;
        private Contact _contactSelected;

        public List<Contact> ContactList { get; set; }
        public Command AddNewContact { get; }
        public MainViewModel(INavigation navigation)
        {
            ContactList = new List<Contact>(Data.GetContacts());
            this._navigation = navigation;
            Title = "MyApp";

            AddNewContact = new Command(GoToAddNewContact);
        }

        private void GoToAddNewContact(object obj)
        {
            _navigation.PushAsync(new AddContactView());
        }

        public Contact ContactSelected
        {
            get => _contactSelected;
            set
            {
                if (_contactSelected != value)
                {
                    _contactSelected = value;
                    if (_contactSelected != null)
                    {
                        _navigation.PushAsync(new DetailView(_contactSelected));
                    }
                }
            }
        }

        

       
            
    }
}
