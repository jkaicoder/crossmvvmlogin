﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CrossMVVMLogin;
using CrossMVVMLogin.Droid;
using SQLite;
using Xamarin.Forms;

[assembly:Dependency(typeof(SQLite_Driod))]
namespace CrossMVVMLogin
{
    public class SQLite_Driod : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            var dbName = "mydb.sqlite";
            var dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(dbName, dbPath);
            var conn = new SQLiteConnection(path);
            return conn;
        }
    }
}